# Uncrustify Build

A repository to generate and house pre-built Uncrustify binaries.

## Editing This README File

Upon editing this file, add `[ci skip]` to the commit message to prevent
unnecessary builds.

## Build

To build, perform the following steps:
* Create a `"build"` subdirectory and switch to it.
* `cmake ..`
* `cmake --build .`

## Release

To create a build release, call:
  ```
  release.sh <build-major>.<build-minor>.<build-patch>
  ```

The build version is distinct from the Uncrustify version it is building due to
GitLab version format limitations. The build version is formulated as follows:
* The build major version corresponds to a unique Uncrustify version. Any
  Uncrustify version change requires a corresponding build major version change.
* The build minor version increments for a potentially breaking change to the
  build code in this repository (for example, changing a build option).
* The build patch version increments for non-breaking changes to the build code
  in this repository.
See the
[releases page](https://gitlab.com/Distributed-Compute-Protocol/uncrustify-build/-/releases)
for existing versions.

The release script will cause a build version tag to be pushed, which in turn
will cause the build artifacts to be uploaded into the
[package registry page](https://gitlab.com/Distributed-Compute-Protocol/uncrustify-build/-/packages),
and the release to appear on the
[releases page](https://gitlab.com/Distributed-Compute-Protocol/uncrustify-build/-/releases).

The
[package registry page](https://gitlab.com/Distributed-Compute-Protocol/uncrustify-build/-/packages)
contains public links to the build packages and SHA256 hash files, which can be
used by any clients wishing to download the builds.
