#!/bin/sh
#
# @file         release.sh
#               Push a tag, resulting in a build release.
# @author       Jason Erb, jason@kingsds.network
# @date         February 2021

set -e

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <major>.<minor>.<patch>" >&2
  exit 1
fi
TAG=$1

cd "$(dirname "$0")"

CHANGES=$(git status --porcelain=v1 2>/dev/null)
if [ ! -z "${CHANGES}" ]
then
  echo "${CHANGES}"
  echo "Uncommitted changes; aborting..."
  exit 1
fi

BRANCH=$(git branch --show-current)
if [ ! "${BRANCH}" = "release" ]
then
  echo "On '${BRANCH}' branch instead of 'release'; aborting..."
  exit 1
fi

VERSION_TAG=$(cat version.txt)
if [ -z "${VERSION_TAG}" ]
then
  echo "Version not found in 'version.txt'; aborting..."
  exit 1
fi

git fetch --tags
git tag -a "${TAG}" HEAD -m "Tag ${TAG} for version ${VERSION_TAG}"
git push origin release "${TAG}"
